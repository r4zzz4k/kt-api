/*
 * Copyright 2019 kt-api contributors. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.r4zzz4k.kt.bitbucket

import io.ktor.client.HttpClient
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.forms.MultiPartFormDataContent
import io.ktor.client.request.forms.formData
import io.ktor.client.request.header
import io.ktor.client.request.request
import io.ktor.http.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import me.r4zzz4k.kt.toBase64

// TODO https://developer.atlassian.com/cloud/bitbucket/oauth-2/
class BitBucket(private val client: HttpClient, serializer: KotlinxSerializer, appUsername: String, appPassword: String) {
    private val baseUrl = "https://api.bitbucket.org/2.0"
    private val basicAuth ="Basic ${"$appUsername:$appPassword".toBase64()}"

    init {
        serializer.apply {
            register(DownloadsResponse.serializer())
            register(DownloadsFile.serializer())
        }
    }

    suspend fun downloads(
        username: String,
        repoSlug: String): DownloadsResponse =
            client.request {
                url {
                    takeFrom("$baseUrl/repositories/$username/$repoSlug/downloads")
                }
                method = HttpMethod.Get
                header("Authorization", basicAuth)
            }

    suspend fun upload(
        username: String,
        repoSlug: String,
        files: List<Pair<String, ByteArray>>): String =
            client.request {
                url {
                    takeFrom("$baseUrl/repositories/$username/$repoSlug/downloads")
                }
                method = HttpMethod.Post
                header("Authorization", basicAuth)
                body = MultiPartFormDataContent(formData {
                    files.forEach { (filename, data) ->
                        append("files", data, headersOf(
                            HttpHeaders.ContentDisposition, "filename=$filename"
                        ))
                        /*append("files", filename) {
                            writeFully(data)
                        }*/
                    }
                })
                println()
            }
}

@Serializable
class DownloadsResponse(
    val values: List<DownloadsFile>
)

@Serializable
class DownloadsFile(
    val name: String,
    @SerialName("created_on")
    val createdOn: String,
    val links: DownloadsLinks
)

@Serializable
class DownloadsLinks(
    val self: Link
)

@Serializable
class Link(
    val href: String
)
