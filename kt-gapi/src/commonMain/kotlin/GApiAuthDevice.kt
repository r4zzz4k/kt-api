/*
 * Copyright 2019 kt-api contributors. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.r4zzz4k.kt.sheets

import io.ktor.client.HttpClient
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.forms.submitForm
import io.ktor.http.Parameters
import kotlinx.serialization.Serializable

// https://developers.google.com/identity/sign-in/devices#obtain_a_user_code_and_verification_url
class GApiAuthDevice(private val client: HttpClient, serializer: KotlinxSerializer) {
    init {
        serializer.apply {
            register(DeviceCode.serializer())
        }
    }

    suspend fun code(clientId: String, scope: String): DeviceCode =
            client.submitForm(SIGNIN_DEVICE_URL, Parameters.build {
                append("client_id", clientId)
                append("scope", scope)
            })
}

@Serializable
data class DeviceCode(
    val deviceCode: String,
    val userCode: String,
    val verificationUrl: String,
    val expiresIn: Int,
    val interval: Int
)

private const val SIGNIN_DEVICE_URL = "https://accounts.google.com/o/oauth2/device/code"
