/*
 * Copyright 2019 kt-api contributors. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.r4zzz4k.kt.sheets

import io.ktor.client.HttpClient
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.*
import io.ktor.http.HttpMethod
import io.ktor.http.takeFrom
import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable

class GApiSheets(private val client: HttpClient, serializer: KotlinxSerializer, private val accessToken: String) {
    private val baseUrl = "https://sheets.googleapis.com/v4/spreadsheets"

    init {
        serializer.apply {
            register(Spreadsheet.serializer())
            register(SpreadsheetProperties.serializer())
            register(Sheet.serializer())
            register(SheetProperties.serializer())
            register(GridData.serializer())
            register(RowData.serializer())
            register(CellData.serializer())
            register(ExtendedValue.serializer())
            register(NamedRange.serializer())
        }
    }

    suspend fun get(
        spreadsheetId: String,
        includeGridData: Boolean = false,
        ranges: List<Pair<String, String>> = emptyList()): Spreadsheet =
        client.request {
            url {
                takeFrom("$baseUrl/$spreadsheetId")
                parameter("includeGridData", includeGridData)
                parameter("ranges", ranges.joinToString(",") { (start, end) -> "$start:$end" })
            }
            method = HttpMethod.Get

            header("Authorization", "Bearer $accessToken")
        }
}

@Serializable
class Spreadsheet(
    val spreadsheetId: String,
    val properties: SpreadsheetProperties,
    val sheets: List<Sheet>,
    //val namedRanges: List<NamedRange>,
    val spreadsheetUrl: String
    //val developerMetadata: List<Map<String, String>>
)

@Serializable
class SpreadsheetProperties(
    val title: String,
    val locale: String
)

@Serializable
class Sheet(
    val properties: SheetProperties,
    val data: List<GridData>
)

@Serializable
class SheetProperties(
    val title: String
)

@Serializable
class GridData(
    //val startRow: Int,
    val rowData: List<RowData>
)

@Serializable
class RowData(
    val values: List<CellData>
)

@Serializable
class CellData(
    @Optional val effectiveValue: ExtendedValue? = null,
    @Optional val formattedValue: String? = null
)

@Serializable
class ExtendedValue(
    val stringValue: String?
)

@Serializable
class NamedRange(

)
