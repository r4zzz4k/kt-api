/*
 * Copyright 2019 kt-api contributors. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.r4zzz4k.kt.sheets

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.client.HttpClient
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.features.origin
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.request.host
import io.ktor.request.port
import io.ktor.response.respondText
import io.ktor.routing.route
import io.ktor.routing.routing
import kotlinx.coroutines.CompletableDeferred

// https://developers.google.com/identity/protocols/OAuth2InstalledApp#creatingcred
class GApiAuthInstalledApp(private val client: HttpClient, serializer: KotlinxSerializer) {
    private val token = CompletableDeferred<String>()

    init {
        serializer.apply {
            register(DeviceCode.serializer())
        }
    }

    suspend fun waitForAccessToken(): String = token.await()

    fun Application.installGApiAuth(client: HttpClient, clientId: String, clientSecret: String, vararg defaultScopes: String) {
        val authProvider = OAuthServerSettings.OAuth2ServerSettings(
            name = "google",
            authorizeUrl = "https://accounts.google.com/o/oauth2/v2/auth",
            accessTokenUrl = "https://www.googleapis.com/oauth2/v4/token",
            requestMethod = HttpMethod.Post,

            clientId = clientId,
            clientSecret = clientSecret,
            defaultScopes = defaultScopes.toList()
        )

        install(Authentication) {
            oauth("google-oauth") {
                this.client = client
                providerLookup = { authProvider }
                urlProvider = { redirectUrl("/login") }
            }
        }

        routing {
            authenticate("google-oauth") {
                route("/login") {
                    handle {
                        val principal = call.authentication.principal<OAuthAccessTokenResponse.OAuth2>() ?: error("No principal")
                        call.respondText(ContentType.Text.Plain) { "Auth succeeded! You can now close browser window" }
                        token.complete(principal.accessToken)
                    }
                }
            }
        }
    }

    private fun ApplicationCall.redirectUrl(path: String): String {
        val defaultPort = if (request.origin.scheme == "http") 80 else 443
        val hostPort = request.host() + request.port().let { port -> if (port == defaultPort) "" else ":$port" }
        val protocol = request.origin.scheme
        return "$protocol://$hostPort$path"
    }
}
