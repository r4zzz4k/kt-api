/*
 * Copyright 2019 kt-api contributors. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.r4zzz4k.kt.slack

import io.ktor.client.HttpClient
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.*
import io.ktor.http.HttpMethod
import io.ktor.http.takeFrom

class Slack(private val client: HttpClient, serializer: KotlinxSerializer, private val accessToken: String) {
    private val baseUrl = "spreadsheets"

    init {
        serializer.apply {
            //register(???.serializer())
        }
    }

    suspend fun get(
        spreadsheetId: String,
        includeGridData: Boolean = false,
        ranges: List<Pair<String, String>> = emptyList()): Unit =
        client.request {
            url {
                takeFrom("$baseUrl/$spreadsheetId")
                parameter("includeGridData", includeGridData)
                parameter("ranges", ranges.joinToString(",") { (start, end) -> "$start:$end" })
            }
            method = HttpMethod.Get

            header("Authorization", "Bearer $accessToken")
        }
}
